import smtplib, socket, sys, getpass
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def obtenerServidorEmail(email):
    dominio = str(email).split("@").__getitem__(1)

    # Formato de salida: (servidor, puerto, usaSSL?)
    if str(dominio).find("outlook") > -1:
        return ("smtp.live.com", 465)

    elif str(dominio).find("hotmail") > -1:
        return ("smtp.live.com", 465)

    elif str(dominio).find("yahoo") > -1:
        return ("smtp.mail.yahoo.com", 465)

    elif str(dominio).find("gmail") > -1:
        return ("smtp.gmail.com", 465)
    else:
        return ("smtp.gmail.com", 465)

def enviarEmail(emailFrom,password,emailTo,subject,message):

    # Conexión
    host = str(list(obtenerServidorEmail(emailFrom)).__getitem__(0))
    port = str(list(obtenerServidorEmail(emailFrom)).__getitem__(1))
    server = None

    # Datos
    mensaje = MIMEMultipart()
    mensaje["From"] = str(emailFrom)
    mensaje["To"] = str(emailTo)
    mensaje["Subject"] = str(subject)
    mensaje.attach(MIMEText(message, "plain"))

    try:
        if port == "465":
            server = smtplib.SMTP_SSL(host, port)
        else:
            server = smtplib.SMTP(host,port)
        
        server.login(emailFrom,password)
        server.sendmail(emailFrom, emailTo, str(mensaje))
    
    except smtplib.SMTPException:
        print(" [Error] No se ha podido autenticar.")
    
    except (socket.gaierror, socket.error, socket.herror, smtplib.SMTPException):
        print(" [Error] Fallo al conectar con el servidor.")
    
    finally:
        server.quit()
        server.close()