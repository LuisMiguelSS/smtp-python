
DROP DATABASE IF EXISTS correo_profesor_luismiguel;

CREATE DATABASE correo_profesor_luismiguel;

USE correo_profesor_luismiguel;

CREATE TABLE alumnado (
    dni CHAR(9) NOT NULL PRIMARY KEY,
    nombre VARCHAR(30),
    direccion VARCHAR(60),
    edad SMALLINT,
    telefono INT(9),
    correo_electronico VARCHAR(50) NOT NULL
);