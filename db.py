import pymysql
import colorama
from colorama import Fore, Style

# Datos de Conexión
DB_HOST = 'localhost'
DB_USER = 'root'
DB_PASS = ''
DB_NAME = 'correo_profesor_luismiguel'
DB_STUDENTS = 'alumnado'

# No Database
def mostrarMensajeError(mensaje):
    colorama.init()
    print(Fore.RED + str(mensaje) + Style.RESET_ALL)


def obtenerTablas():
    colorama.init()

    tablas = list()
    cursor = None
    conexion = None

    try:
        conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)

        cursor = conexion.cursor()
        cursor.execute("USE " + DB_NAME)
        cursor.execute("SHOW TABLES")

        tablas = [tabla for (tabla,) in cursor]

    except:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos.")
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()

    return tablas

# Existe tabla
def existeTabla(tabla):
    for tab in obtenerTablas():
        if tab == str(tabla):
            return True

    return False

def existeDni(dni):
    colorama.init()
    
    if not existeTabla(DB_STUDENTS):
        return False

    cursor = None
    conexion = None
    resultado = False

    try:
        conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)

        cursor = conexion.cursor()
        resultado = cursor.execute("SELECT dni FROM  " + DB_STUDENTS + " WHERE dni = \'" + str(dni).lower() + "\'") > 0


    except:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos.")
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()

    return resultado

# INSERTAR
def insertarAlumno(dni, nombre, direccion, edad, telefono, correo_electronico):
    colorama.init()

    conexion = None
    cursor = None
    result = None

    try:
        # Comprobar que los valores recibidos sean correctos
        if not existeTabla(DB_STUDENTS):
            return 0

        # Inicializar conexión
        conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
        cursor = conexion.cursor()

        query = "INSERT INTO " + DB_STUDENTS + " VALUES (%s, %s, %s, %s, %s, %s)"
        valores = (dni,nombre,direccion,edad,telefono,correo_electronico)

        # Ejecutar consulta
        result = cursor.execute(query, valores)
        conexion.commit()
    
    except pymysql.err.IntegrityError:
        mostrarMensajeError(" [Error] La clave primaria ya existe, no se ha podido añadir.")
    except Exception as e:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos." + str(e))
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()

        return result if result != None else 0

# MODIFICAR
def modificarAlumno(dniAlumno,nuevoNombre,nuevaDireccion,nuevaEdad,nuevoTelefono,nuevoCorreo):
    colorama.init()

    conexion = None
    cursor = None
    result = None

    try:
        # Comprobar que los valores recibidos sean correctos
        if not existeTabla(DB_STUDENTS):
            return 0

        # Inicializar conexión
        conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
        cursor = conexion.cursor()

        query = "UPDATE " + DB_STUDENTS + " SET nombre = %s, direccion = %s, edad = %s, telefono = %s, correo_electronico = %s WHERE dni = %s"

        # Ejecutar consulta
        result = cursor.execute(query, (nuevoNombre,nuevaDireccion,nuevaEdad,nuevoTelefono,nuevoCorreo, dniAlumno))
        conexion.commit()
    
    except pymysql.err.IntegrityError as ie:
        mostrarMensajeError(" [Error] " + str(ie))
    except Exception as e:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos." + str(e))
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()

        return result if result != None else 0

# ELIMINAR
def eliminarAlumno(dni):
    colorama.init()

    conexion = None
    cursor = None
    result = None

    try:
        # Comprobar que los valores recibidos sean correctos
        if not existeTabla(DB_STUDENTS):
            return 0

        # Inicializar conexión
        conexion = pymysql.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
        cursor = conexion.cursor()

        query = "DELETE FROM " + DB_STUDENTS + " WHERE dni = %s"

        # Ejecutar consulta
        result = cursor.execute(query, dni)
        conexion.commit()
    
    except pymysql.err.IntegrityError as ie:
        mostrarMensajeError(" [Error] " + str(ie))
    except Exception as e:
        mostrarMensajeError(" [Error] Ha ocurrido un error conectando con la base de datos." + str(e))
    finally:
        # Cerrar conexiones
        if cursor is not None:
            cursor.close()
        if conexion is not None:
            conexion.close()

        return result if result != None else 0