import smtplib, socket, sys, getpass
import colorama
import re
import os
import getpass
from colorama import Fore, Style
from db import insertarAlumno, existeDni, modificarAlumno, eliminarAlumno
from gestor_correo import enviarEmail

caracterSalida = "Q"
correoEstablecido = ""
contrasenia = ""

# Regular Expresssions
RX_DNI = r"\d{8}[A-z]"
RX_PHONE = r"\d{9}"
RX_EMAIL = r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" # According to RFC2822

# Funciones

def limpiarPantalla():
    os.system('cls' if os.name == 'nt' else 'clear')

def readInput(message):
    salidaDelPrograma = False
    try:
        return input(message)
    except KeyboardInterrupt:
        mostrarMensajeInformacion(" [Info.] Pulse Ctrl+c de nuevo para salir...")
        salidaDelPrograma = True
    except EOFError:
        mostrarMensajeAviso(" [Aviso] El valor introducido no es válido.")
    
    if salidaDelPrograma:
        try:
            return input(message)
        except EOFError:
            return readInput(message)
    else:
        return readInput(message)

def mostrarMensajeInformacion(mensaje):
    print(Fore.CYAN + str(mensaje) + Style.RESET_ALL)

def mostrarMensajeExito(mensaje):
    print(Fore.GREEN + str(mensaje) + Style.RESET_ALL)

def mostrarMensajeAviso(mensaje):
    print(Fore.YELLOW + str(mensaje) + Style.RESET_ALL)

def mostrarMensajeError(mensaje):
    print(Fore.RED + str(mensaje) + Style.RESET_ALL)

def mostrarMenuInicial():
    print("********** AULA PYTHON **********")
    print(" 1) Gestión de alumnos")
    print(" 2) Enviar correo")

def mostrarMenuGestionAlumnos():
    print("****** Gestión Alumnos ******")
    print(" 1. Añadir alumno")
    print(" 2. Modificar alumno")
    print( "3. Eliminar alumno")

def mostrarMenuEnviarCorreo():
    print("****** Enviar Correo-E ******")
    print(" 1. Configuración del correo")

    if correoEstablecido:
        print(" 2.Enviar correo")
    else:
        print(Fore.LIGHTBLACK_EX + " 2. Enviar correo" + Style.RESET_ALL)

def mostrarResumenEmail(emailFrom,emailTo,subject,body):
    print("Resumen del correo:")
    print("|--------------------------------|")
    print("|De  : " + str(emailFrom))
    print("|Para: " + str(emailTo))
    print("|--------------------------------|")
    print("|Asunto: " + str(asunto))
    print("|--------------------------------|")
    print("|Cuerpo del mensaje:")
    
    if len(str(body).split("\n")) > 1:
        for linea in body.split("\n"):
            print("|" + str(linea))
    else:
        print("|" + str(body))
    
    print("|--------------------------------|")

def solicitarOpcion(minimo,maximo):
    cadena = " -Seleccione opción (" + str(minimo) + "-" + str(maximo) + "), \"" + caracterSalida + "\" para salir: "
    return readInput(cadena)

# MAIN
colorama.init()

entrada = ""
try:
    limpiarPantalla()
    while entrada != caracterSalida:
        mostrarMenuInicial()
        entrada = solicitarOpcion(1,2)

        # Comprobar que la entrada sea numérica
        if entrada.isdigit() == False:
            continue

        if entrada == "1":
            limpiarPantalla()
            mostrarMenuGestionAlumnos()
            entrada = solicitarOpcion(1,3)

            if entrada.isdigit():
                # Insertar nuevo alumno
                if entrada == "1":

                    # DNI
                    while re.search(RX_DNI,entrada) == None:
                        entrada = readInput(" -DNI: ")
                    dni = entrada

                    # Nombre
                    entrada = "1"
                    while entrada.isdigit() and entrada.strip() != "":
                        entrada = readInput(" -Nombre: ")
                    nombre = entrada

                    # Dirección
                    entrada = "1"
                    while entrada.isdigit() and entrada.strip() != "":
                        entrada = readInput(" -Dirección: ")
                    direccion = entrada

                    # Edad
                    while not entrada.isnumeric() or int(entrada) < 1:
                        entrada = readInput(" -Edad (1+): ")
                    edad = entrada

                    # Teléfono
                    entrada = ""
                    while re.search(RX_PHONE,entrada) == None:
                        entrada = readInput(" -Número de teléfono (9 números sin espacios): ")
                    telefono = entrada

                    # Correo electrónico
                    while re.search(RX_EMAIL,entrada) == None:
                        entrada = readInput(" -Correo electrónico: ")
                    correo = entrada

                    limpiarPantalla()
                    print("¿Está seguro de que desea añadir al alumno con la siguiente información?:\n")
                    print(" ·Dni:       " + str(dni))
                    print(" ·Nombre:    " + str(nombre))
                    print(" ·Dirección: " + str(direccion))
                    print(" ·Edad:      " + str(edad))
                    print(" ·Teléfono:  " + str(telefono))
                    print(" ·Correo-e:  " + str(correo))

                    entrada = readInput("  (Sí/no): ")

                    # Comprobar si el usuario ha confirmado la inserción
                    if entrada.lower() == "sí" or entrada.lower() == "si" or entrada.lower() == "s":
                        if insertarAlumno(dni,nombre,direccion,edad,telefono,correo) > 0:
                            mostrarMensajeExito("Alumno añadido.")
                        else:
                            mostrarMensajeError(" [Error] No se ha podido añadir al alumno.")
                    else:
                        mostrarMensajeInformacion(" [Info.] Se ha cancelado.")
                
                elif entrada == "2":
                    # Modificar alumno

                    while( not re.search(RX_DNI,entrada.strip()) ):
                        entrada = readInput(" -Introduzca el dni del alumno a modificar: ")
                    
                    if not existeDni(entrada.strip()):
                        mostrarMensajeError(" [Error] El dni introducido no existe.")
                        continue

                    dni = entrada.strip()

                    # Nombre
                    entrada = "1"
                    while entrada.isdigit() and entrada.strip() != "":
                        entrada = readInput(" -Nombre: ")
                    nombre = entrada

                    # Dirección
                    entrada = "1"
                    while entrada.isdigit() and entrada.strip() != "":
                        entrada = readInput(" -Dirección: ")
                    direccion = entrada

                    # Edad
                    while not entrada.isnumeric() or int(entrada) < 1:
                        entrada = readInput(" -Edad (1+): ")
                    edad = entrada

                    # Teléfono
                    entrada = ""
                    while re.search(RX_PHONE,entrada) == None:
                        entrada = readInput(" -Número de teléfono (9 números sin espacios): ")
                    telefono = entrada

                    # Correo electrónico
                    while re.search(RX_EMAIL,entrada) == None:
                        entrada = readInput(" -Correo electrónico: ")
                    correo = entrada

                    # Solicitar confirmación
                    limpiarPantalla()
                    print("¿Está seguro de que desea realizar las siguientes modificaciones?:\n")
                    print(" ·Nombre:    " + str(nombre))
                    print(" ·Dirección: " + str(direccion))
                    print(" ·Edad:      " + str(edad))
                    print(" ·Teléfono:  " + str(telefono))
                    print(" ·Correo-e:  " + str(correo))

                    entrada = readInput("  (Sí/no): ")

                    # Comprobar si el usuario ha confirmado la inserción
                    if entrada.lower() == "sí" or entrada.lower() == "si" or entrada.lower() == "s":
                        if modificarAlumno(dni,nombre,direccion,edad,telefono,correo) > 0:
                            mostrarMensajeExito("Alumno modificado.")
                        else:
                            mostrarMensajeError(" [Error] No se ha podido modificar el alumno.")
                    else:
                        mostrarMensajeInformacion(" [Info.] Se ha cancelado.")
                
                elif entrada == "3":
                    # Eliminar alumno

                    while( not re.search(RX_DNI,entrada.strip()) ):
                        entrada = readInput(" -Introduzca el dni del alumno a modificar: ")
                    
                    if not existeDni(entrada.strip()):
                        mostrarMensajeError(" [Error] El dni introducido no existe.")
                        continue

                    dni = entrada.strip()
                    entrada = readInput(" ¿Está seguro de que desea eliminar el registro? (Sí/no): ")

                    # Comprobar si el usuario ha confirmado la inserción
                    if entrada.lower() == "sí" or entrada.lower() == "si" or entrada.lower() == "s":
                        if eliminarAlumno(dni) > 0:
                            mostrarMensajeExito("Alumno eliminado.")
                        else:
                            mostrarMensajeError(" [Error] No se ha podido eliminar el alumno.")
                    else:
                        mostrarMensajeInformacion(" [Info.] Se ha cancelado.")


            else:
                mostrarMensajeAviso(" [Aviso] Debe introducir un número.")

        elif entrada == "2":
            limpiarPantalla()

            # Correo
            if correoEstablecido != "":
                entrada = ""
                while (entrada != "1" and entrada != "2"):
                    mostrarMenuEnviarCorreo()
                    entrada = solicitarOpcion(1,2)

                if entrada == "1":
                    # Reconfigurar email
                    print(" ·Configuración del Correo Electrónico:")

                    # Correo
                    while re.match(RX_EMAIL, entrada) == None:
                        entrada = readInput(" -Modificar dirección de correo: ")
                    correoEstablecido = entrada

                    # Contraseña
                    entrada = ""
                    while entrada.strip() == "":
                        entrada = getpass.getpass(" -Contraseña: ")
                    contrasenia = entrada

                    mostrarMensajeExito("Nuevo correo electrónico (" + correoEstablecido + ") establecido.")

                elif entrada == "2":
                    # Enviar correo
                    while re.match(RX_EMAIL, entrada) == None:
                        entrada = readInput(" -Correo electrónico destino: ")
                    emailDestino = entrada

                    # Asunto
                    entrada = ""
                    while entrada.strip() == "":
                        entrada = readInput("-Asunto: ")
                    asunto = entrada

                    # Cuerpo
                    entrada = ""
                    numeroMaxLineas = 5
                    lineaActual = 1
                    cuerpoMensaje = ""
                    while entrada.strip() == "" and lineaActual <= numeroMaxLineas:
                        
                        if lineaActual == 1:
                            entrada = readInput("-Cuerpo del mensaje (máx. " + str(numeroMaxLineas) + " líneas): ")

                            if entrada.strip() == "":
                                continue
                            else:
                                lineaActual += 1
                                cuerpoMensaje += entrada
                                entrada = ""
                        else:
                            entrada = readInput("")
                            lineaActual += 1
                            cuerpoMensaje += "\n" + entrada
                            entrada = ""
                    
                    limpiarPantalla()
                    mostrarResumenEmail(correoEstablecido,emailDestino,asunto,cuerpoMensaje)

                    # Confirmación de envío del email.
                    entrada = readInput(" ¿Está seguro de enviar el email? (Sí/no): ")
                    if entrada.lower() == "sí" or entrada.lower() == "si" or entrada.lower() == "s":
                        enviarEmail(correoEstablecido,contrasenia,emailDestino,asunto,cuerpoMensaje)
                        mostrarMensajeExito(" Correo enviado.")
                    else:
                        mostrarMensajeAviso(" Envío del mensaje cancelado.")

            else:
                while entrada != "1":
                    mostrarMenuEnviarCorreo()
                    entrada = solicitarOpcion(1,1)

                if entrada != caracterSalida:
                    print(" ·Configuración del Correo Electrónico:")
                    while re.match(RX_EMAIL, entrada) == None:
                        entrada = readInput(" -Nueva dirección de correo: ")
                    correoEstablecido = entrada
                    
                    # Contraseña
                    entrada = ""
                    while entrada.strip() == "":
                        entrada = getpass.getpass(" -Contraseña: ")
                    contrasenia = entrada
                    
                    mostrarMensajeExito("Nuevo correo electrónico (" + correoEstablecido + ") establecido.")

except KeyboardInterrupt:
    mostrarMensajeAviso("Saliendo...")